# Notes 

## 2021-01-29
- Realized that I don't have following keys:
  - +
  - ~

I have also accidently pressed the media keys in RAISE layer, so perhaps
move these to another layer or remove them since I rarily use such keys...

## 2021-01-25
- Moved shifted keys {,},[,] down one row and added VIM-movement keys (e.g. H -> Left)
- Maybe replace arrow keys on base layer with something else?
  - Move enter key to bottom right corner in base layer

## 2021-01-21
- Added F-key cluster on RAISE layer
- Added Numeric cluster on RAIS layer
- Reworked LOWER layer (inspired by https://configure.ergodox-ez.com/planck-ez/layouts/XElw4/latest/2)

## 2021-01-18
Notes from the first day using the keymap.

- Maybe need to switch place on backspace and raise
- Hard to pinpoint specific numeric key.
  - Perhaps make a cluster like 4x4 starting on Q for F-keys
    - Do the same but on righthand side for numeric input?

- Switch place on backspace and raise?