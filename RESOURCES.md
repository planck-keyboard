## Online inspirational resources
- https://configure.ergodox-ez.com/planck-ez/search?q=swedish
  + https://configure.ergodox-ez.com/planck-ez/layouts/XElw4/latest/0
- https://andecho.com/tag/planck/
- https://www.reddit.com/r/olkb/comments/c7zdh4/nordic_swedish_offbyone_iso_12x4a_plancks/
- http://www.keyboard-layout-editor.com/#/layouts/44e38f3063dad1063acaa7937712996d
- http://www.keyboard-layout-editor.com/#/layouts/ca7723e3dd86efa70b3bdb885da47a8f
- https://www.reddit.com/r/olkb/comments/88xv5w/any_isousers_with_planck_or_preonic_layout_help/
- Johannes keymap/layout (in QMK repo). **Uses RALT(KC_W) for Å, RALT(KC_Q) for Ä and RALT(KC_P) for Ö**

## Common keys related
- Check out the Raise layer (mimics the placement of parentheses etc. at top row... should be quite easy to adapt to?)
  - https://configure.ergodox-ez.com/planck-ez/layouts/XElw4/latest/2

## Navigation related
- See Nav 4 layer (especially the mouse cluster) 
  - https://configure.ergodox-ez.com/planck-ez/layouts/AB4eG/latest/4



